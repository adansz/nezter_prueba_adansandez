
import { Injectable } from '@angular/core';
import { Http,RequestOptions,Headers } from '@angular/http';
import { HttpClient, HttpHeaders } from "@angular/common/http";

/*
  Generated class for the CrudProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CrudProvider {
  url:any;
  headers = new Headers({ 'Content-Type': 'application/json' });  
  _options = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
  options = new RequestOptions({ headers: this.headers });  
  constructor(public http: HttpClient) {
    console.log('Hello CrudProvider Provider');
    this.url = "http://192.168.12.53:5000/";
  }

  crearUsuario(data){
    console.log(data);
    let json = JSON.stringify(data);    
    return new Promise(resolve =>{
      this.http.post(this.url+"nuevoUsuario?dato=2323&dato2=23",json,this._options)
      .subscribe(res => {
        resolve(res);
      })
    })
  }

  iniciar_sesion(data){
    console.log(data);
    let json = JSON.stringify(data);    
    return new Promise(resolve =>{
      this.http.post(this.url+"iniciarSesion",json,this._options)
      .subscribe(res => {
        resolve(res[0]);        
      })
    })
  }

  getData(){
    // let json = JSON.stringify(data);
    return new Promise(resolve => {
      this.http.get(this.url+"getData")        
        .subscribe(data => {
          resolve(data);
        })
    });
  }

  editar(item){
    console.log(item);
    let json = JSON.stringify(item);    
    return new Promise(resolve =>{
      this.http.post(this.url+"editar",json,this._options)
      .subscribe(res => {
        resolve(res[0]);     

      })
    })
  }

  deleteItem(item){
    console.log(item);
    let json = JSON.stringify(item);    
    return new Promise(resolve =>{
      this.http.post(this.url+"eliminarItem",json,this._options)
      .subscribe(res => {
        resolve(res[0]);     

      })
    })
  }


}
