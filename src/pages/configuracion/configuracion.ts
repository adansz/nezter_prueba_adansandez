import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CrudProvider } from "../../providers/crud/crud";
/**
 * Generated class for the ConfiguracionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-configuracion',
  templateUrl: 'configuracion.html',
})
export class ConfiguracionPage {
  data:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public servicio:CrudProvider) {
    this.data = this.navParams.get("items");
    console.log(this.navParams.get("items"));
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ConfiguracionPage');
  }

  guardar(){
    if(confirm("Desea editar")){
      this.servicio.editar(this.data).
      then(res =>{
        alert("Editado con exito");
        this.navCtrl.pop();
      })
    }
  }

  borrar(){
    if(confirm("Desea eliminar")){
      this.servicio.deleteItem(this.data).
      then(res =>{
        alert("Borrado con exito");
        this.navCtrl.popAll();
      })
    }
  }
}
