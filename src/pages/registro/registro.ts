import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CrudProvider } from "../../providers/crud/crud";
import { MainPage } from '../main/main';
/**
 * Generated class for the RegistroPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-registro',
  templateUrl: 'registro.html',
})
export class RegistroPage {
  frm:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public servicio:CrudProvider) {
    this.frm = {};
    this.frm.pass1 = "";
    this.frm.pass2 = "";
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegistroPage');
  }

  crear(){
    if(this.frm.pass1 != "" && this.frm.pass2 != ""){      
      if(this.frm.pass1 == this.frm.pass2){
        if(this.validateEmail(this.frm.email)){
          this.servicio.crearUsuario(this.frm)
          .then(res =>{
            if(res["rowsAffected"][0]){
              this.navCtrl.setRoot(MainPage); 
            }            
          })
        }
      }
    }
    
  }

  validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  editar(){

  }

  borrar(){

  }

}
