import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { RegistroPage } from '../registro/registro';
import { MainPage } from '../main/main';
import { CrudProvider } from "../../providers/crud/crud";
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  frm:any;
  constructor(public navCtrl: NavController, public servicio:CrudProvider) {
    this.frm = {};
    this.frm.user = "";
  }

  sesion(){
    console.log("sesion");
    console.log(this.frm);
    this.servicio.iniciar_sesion(this.frm)
    .then(res => {
      console.log(res);
      if(res){
        this.navCtrl.setRoot(MainPage);
      }
    })
    
  }

  goToRegistro(){
    this.navCtrl.push(RegistroPage);
  }
}
