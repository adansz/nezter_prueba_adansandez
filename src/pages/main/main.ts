import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CrudProvider } from "../../providers/crud/crud";
import { ConfiguracionPage } from "../configuracion/configuracion";
/**
 * Generated class for the MainPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-main',
  templateUrl: 'main.html',
})
export class MainPage {
  data:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public servicio:CrudProvider) {
    this.data = [];    
    this.servicio.getData().
    then(res =>{
      this.data = res[0];
      console.log(res);
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MainPage');
  }

  configuracion(items:any){
    this.navCtrl.push(ConfiguracionPage,{"items":items});
  }

}
